package com.example.demoJwt.config;

import com.example.demoJwt.services.DatabaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.servlet.PathRequest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityCustomizer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@EnableWebSecurity
public class SecurityConfig {

	@Autowired
	private UserDetailsService userDetailsService; // Service pour gérer les détails des utilisateurs

	@Autowired
	private JwtAuthFilter jwtAuthFilter; // Filtre d'authentification basé sur JWT

	@Bean
	public SecurityFilterChain securityFilterChain(HttpSecurity httpSecurity) throws Exception {
		return httpSecurity
				.csrf().disable()
				.authorizeHttpRequests(authorizeRequests ->
						authorizeRequests

								//.requestMatchers(PathRequest.toStaticResources().atCommonLocations()).permitAll() // Autorise les ressources statiques
								.requestMatchers("/login","/web/visiteur","/web/visiteur/**").permitAll() // Autorise les URL de /web/visiteur sans authentification
								.requestMatchers("/web/client/**").hasAnyAuthority("Client", "Admin") // Exige "Client" ou "Admin" pour /web/client
								.requestMatchers("/web/admin/**").hasAuthority("Admin") // Exige "Admin" pour /web/admin
								.requestMatchers("/api/visiteur/**").permitAll() // Autorise toutes les URL de /api sans authentification
								.requestMatchers("/api/client/**").hasAnyAuthority("Client", "Admin")
								.requestMatchers("/api/admin/**").hasAuthority("Admin")
								.requestMatchers("/auth/**").permitAll() // Autorise les URL publiques sans authentification
								.anyRequest().authenticated() // Exige l'authentification pour toutes les autres requêtes
				)
				.formLogin().loginPage("/login").defaultSuccessUrl("/web/visiteur")
				.and()
				.sessionManagement()
				.sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED)
				.and()
				.exceptionHandling()
				.accessDeniedPage("/error/403") // Rediriger vers la page d'erreur 403 en cas d'accès refusé
				.and()
				.authenticationProvider(authenticationProvider())
				.addFilterBefore(jwtAuthFilter, UsernamePasswordAuthenticationFilter.class)
				.build();
	}


	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder(); // Utilise l'encodeur BCrypt pour les mots de passe
	}

	@Bean
	public AuthenticationManager authenticationManager(AuthenticationConfiguration config) throws Exception {
		return config.getAuthenticationManager(); // Configure le gestionnaire d'authentification
	}

	@Bean
	public WebSecurityCustomizer webSecurityCustomizer() {
		return (web) -> web.ignoring().requestMatchers(new AntPathRequestMatcher("/h2-console/**"));
		// Permet d'accéder à la console H2 sans authentification (utile pour le développement)
	}

	private AuthenticationProvider authenticationProvider() {
		final DaoAuthenticationProvider authenticationProvider = new DaoAuthenticationProvider();
		authenticationProvider.setUserDetailsService(userDetailsService);
		authenticationProvider.setPasswordEncoder(passwordEncoder());
		return authenticationProvider; // Configure le fournisseur d'authentification pour l'application
	}
}

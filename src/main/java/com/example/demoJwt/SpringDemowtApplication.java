package com.example.demoJwt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringDemowtApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringDemowtApplication.class, args);
	}

}

package com.example.demoJwt.entities;

import java.io.Serializable;

import jakarta.persistence.*;

import com.example.demoJwt.dtos.UtilisateurDTO;


/**
 * Cette classe représente un objet Utilisateur dans la base de données.
 */
@Entity
public class Utilisateur implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(nullable = false, length = 50)
	private String name;

	@Column(nullable = false, unique = true, length = 100)
	private String email;

	@Column(nullable = false, length = 60)
	private String password;

	@ManyToOne
	@JoinColumn(name = "role_id")
	private Role role;

	/**
	 * Constructeur par défaut de la classe Utilisateur.
	 */
	public Utilisateur() {
		super();
	}

	/**
	 * Constructeur de la classe Utilisateur avec des paramètres.
	 *
	 * @param name     Le nom de l'utilisateur.
	 * @param email    L'adresse e-mail de l'utilisateur.
	 * @param password Le mot de passe de l'utilisateur.
	 */
	public Utilisateur(String name, String email, String password) {
		super();
		this.name = name;
		this.email = email;
		this.password = password;
	}

	/**
	 * Constructeur de la classe Utilisateur avec des paramètres, y compris le rôle.
	 *
	 * @param id       L'identifiant de l'utilisateur.
	 * @param name     Le nom de l'utilisateur.
	 * @param email    L'adresse e-mail de l'utilisateur.
	 * @param password Le mot de passe de l'utilisateur.
	 * @param role     Le rôle de l'utilisateur.
	 */
	public Utilisateur(Long id, String name, String email, String password, Role role) {
		this.id = id;
		this.name = name;
		this.email = email;
		this.password = password;
		this.role = role;
	}

	/**
	 * Constructeur de la classe Utilisateur à partir d'un objet UtilisateurDTO.
	 *
	 * @param utilisateurDTO L'objet UtilisateurDTO à partir duquel initialiser l'utilisateur.
	 */
	public Utilisateur(UtilisateurDTO utilisateurDTO) {
		this.id = utilisateurDTO.getId();
		this.name = utilisateurDTO.getName();
		this.email = utilisateurDTO.getEmail();
		this.password = utilisateurDTO.getPassword();
		this.role = utilisateurDTO.getRole();
	}

	// Getters et Setters

	// Méthodes Getter et Setter pour les attributs id, name, email, password et role.

	@Override
	public String toString() {
		return "Utilisateur{" +
				"id=" + id +
				", name='" + name + '\'' +
				", email='" + email + '\'' +
				", role=" + role +
				'}';
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}
}

package com.example.demoJwt.services;

import com.example.demoJwt.entities.Utilisateur;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	private UtilisateurService utilisateurService; // Service pour gérer les détails des utilisateurs

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		// Méthode pour charger les détails de l'utilisateur en fonction de son nom d'utilisateur (e-mail)

		try {
			Utilisateur utilisateur = utilisateurService.findByEmail(username);
			// Récupère les informations de l'utilisateur à partir du service utilisateur

			String nomRole = utilisateur.getRole().getNom();
			// Récupère le nom du rôle de l'utilisateur

			List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
			authorities.add(new SimpleGrantedAuthority(nomRole));
			// Crée une liste d'autorités (rôles) pour l'utilisateur basée sur son rôle

			return new User(username, utilisateur.getPassword(), authorities);
			// Crée un objet UserDetails qui représente l'utilisateur avec son nom d'utilisateur, son mot de passe
			// et ses autorités (rôles)
		} catch (Exception e) {
			throw new UsernameNotFoundException(e.getMessage());
			// En cas d'erreur, lance une exception UsernameNotFoundException
		}
	}
}

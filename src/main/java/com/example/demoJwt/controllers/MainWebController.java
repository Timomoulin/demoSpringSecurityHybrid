package com.example.demoJwt.controllers;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MainWebController {
    @GetMapping("/login")
    public String login(){
        return "visiteur/login";
    }
    @GetMapping("/web/visiteur")
    public String visiteurPage() {
        return "visiteur/index";
    }

    @GetMapping("/web/client")
    public String clientPage() {
        return "client/index";
    }

    @GetMapping("/web/admin")
    public String adminPage() {
        return "admin/index";
    }
}

package com.example.demoJwt.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demoJwt.dtos.UtilisateurDTO;
import com.example.demoJwt.entities.Utilisateur;
import com.example.demoJwt.services.UtilisateurService;

@RestController
@RequestMapping("/api/person")
public class PersonController {
	
	@Autowired
	private UtilisateurService service;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@GetMapping
	public ResponseEntity<List<UtilisateurDTO>> findAll() {
		final List<Utilisateur> utilisateurs = service.findAll();
		final List<UtilisateurDTO> dtos = utilisateurs.stream().map(p -> new UtilisateurDTO(p)).toList();
		return ResponseEntity.ok(dtos);
	}
	
	public ResponseEntity<UtilisateurDTO> create(@RequestBody UtilisateurDTO dto) {
		dto.setPassword(passwordEncoder.encode(dto.getPassword()));
		return ResponseEntity.ok(service.create(dto));
	}
	
	public ResponseEntity<UtilisateurDTO> update(@RequestBody UtilisateurDTO dto) {
		dto.setPassword(passwordEncoder.encode(dto.getPassword()));
		return ResponseEntity.ok(service.create(dto));
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> delete(@PathVariable Long id) {
		service.delete(id);
		return ResponseEntity.noContent().build();
	}

}

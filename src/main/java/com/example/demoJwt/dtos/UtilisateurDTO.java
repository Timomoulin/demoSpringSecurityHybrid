package com.example.demoJwt.dtos;

import com.example.demoJwt.entities.Role;
import com.example.demoJwt.entities.Utilisateur;


public class UtilisateurDTO {
	
	private Long id;
	private String name;
	private String email;
	private String password;
	private Role role;
	
	public UtilisateurDTO() {
		super();
	}

	public UtilisateurDTO(Long id, String name, String email, Role role) {
		super();
		this.id = id;
		this.name = name;
		this.email = email;
		this.role=role;
	}
	
	public UtilisateurDTO(Utilisateur utilisateur) {
		super();
		this.id = utilisateur.getId();
		this.name = utilisateur.getName();
		this.email = utilisateur.getEmail();
		this.role=utilisateur.getRole();
	}

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getEmail() {
		return email;
	}

	public String getPassword() {
		return password;
	}
	

	public void setId(Long id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}
}
